import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import { ApolloError } from "apollo-server-koa";
import UUID from 'uuid/v1';

const APP_SECRET = "HACKNIZER_TOP_SECRET_CARA";

const validate = (args) => {
  for (let key in args)
    if (!args[key])
      throw new ApolloError(`${key} cannot be blank`, 400);
}

export default {
  Query: {
    async user(parent, args, context, info) {
      const { User } = context.db;

      await User.sync();
      const token = context.request.header.authorization.replace(/Bearer /, "");
      const decodedInfo = jwt.verify(token, APP_SECRET);

      return await User.findById(decodedInfo.userId);
    }
  },
  Mutation: {
    async signup(parent, args, context, info) {
      const { User } = context.db;

      if (args.password !== args.passwordConfirmation)
        throw new ApolloError("Passwords don't match", 400);

      const password = await bcrypt.hash(args.password, 10);

      await User.sync();
      let user = await User.findOne({
        where: {
          email: args.email
        }
      });

      if (user)
        throw new ApolloError("Email already exists", 400);

      const hash = bcrypt.hashSync(args.password, 10);

      user = {
        id: UUID(),
        name: args.name,
        email: args.email,
        password: hash
      };

      if (process.env.NATS_PRESENT !== 'false') {
        const userStr = JSON.stringify(user);
        const buff = Buffer.allocUnsafe(userStr.length);
        buff.fill(userStr);

        try {
          await context.nats.publish("new_user", buff);
        } catch(err) {
          console.log(err.stack);
        }
        return user;
      } else {
        user = await User.create(user);
        return user.dataValues;
      }
    },

    async login(parent, args, context, info) {
      const { User } = context.db;

      const user = await User.findOne({
        where: {
          email: args.email
        }
      })

      if (!user)
        throw new ApolloError("Email not found", 404);

      const valid = await bcrypt.compare(args.password, user.dataValues.password);

      if (!valid)
        throw new ApolloError("Wrong Password", 401);

      return jwt.sign({
        userId: user.dataValues.id
      }, APP_SECRET);
    },

    async updateProfile(parent, args, context, info) {
      const { User } = context.db;

      await User.update(
        {
          name: args.name,
          biography: args.biography,
          avatar: args.avatar
        },
        {where: {id: args.id}}
      )
        .then(result => {
          console.log("User updated");
        })
        .catch(err => {
          console.log(err);
        });

        return await User.findById(args.id);

    }
  }
}