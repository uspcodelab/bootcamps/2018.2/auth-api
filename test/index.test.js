import jwt from "jsonwebtoken";
import db from '../src/db/models';
import resolver from '../src/graphql/resolvers';

const APP_SECRET = "HACKNIZER_TOP_SECRET_CARA";

const context = {
    db
};

beforeAll(async () => {
   await db.User.create({
       id: '234517c0-f4c7-11e8-8ec8-a7d07900be5a',
        name: 'Leonardo Lana',
        password: '12345678',
        passwordConfirmation: '12345678',
        email: 'lana@usp.br'
   }) 
})

afterAll(async () => {
    await db.User.destroy({
        where:{}
    });

    db.sequelize.close();
})

test('Create a user', async () => {
    const args = {
        name: 'Victor João',
        password: '12345678',
        passwordConfirmation: '12345678',
        email: 'victor.joao@usp.br'
    };

    const user = await resolver.Mutation.signup(null, args, context, null); 
    expect(user.id).toBeDefined();
})

test('Login a user', async () => {
    const args = {
        email: 'victor.joao@usp.br',
        password: '12345678'
    }

    const login = await resolver.Mutation.login(null, args, context, null);
    expect(login).toBeDefined();
});

test('Update profile', async () => {
    const args = {
        id: '234517c0-f4c7-11e8-8ec8-a7d07900be5a',
        name: 'Victor Martins João',
        avatar: 'Fotinho',
        biography: 'Sou uma pessoa legal'
    }

    const userUpdated = await resolver.Mutation.updateProfile(null, args, context, null);
    expect(userUpdated).toBeDefined();
})

test('Query a user', async () => {

    const token = jwt.sign({
        userId: '234517c0-f4c7-11e8-8ec8-a7d07900be5a'
      }, APP_SECRET);

    context.request = {
        header: {
            authorization: 'Bearer ' + token
        }
    }
    
    const user = await resolver.Query.user(null, null, context, null);
    expect(user).toBeDefined();
})

